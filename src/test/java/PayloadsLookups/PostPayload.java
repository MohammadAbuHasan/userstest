package PayloadsLookups;

public class PostPayload {
    String username,Password,ID;

    public PostPayload(String username, String password, String ID) {
        this.username = username;
        Password = password;
        this.ID = ID;
    }


    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return Password;
    }

    public String getID() {
        return ID;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public void setID(String ID) {
        this.ID = ID;
    }
}
