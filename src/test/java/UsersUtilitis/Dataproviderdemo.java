package UsersUtilitis;

import Usersbase.userbase;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.DataProvider;

import java.io.IOException;

public class Dataproviderdemo  {

    private String filePath = "C:\\Users\\Mohammad\\IdeaProjects\\USERS\\src\\test\\java\\UsersUtilitis\\demo.xlsx";

    private String sheetName = userbase.sheetName;
    @DataProvider(name="excelData")
    public Object[][] readExcel() throws InvalidFormatException, IOException {
        return ExcelReader.readExcel(filePath, sheetName);
    }

}
