package Testcaseuasers;

import Usersbase.userbase;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC1_Get_All_Users extends userbase {

    @BeforeClass
    void getAllUsers() throws InterruptedException{
        RestAssured.baseURI="https://fakerestapi.azurewebsites.net/api";
        httpRequest=RestAssured.given();
        response=httpRequest.request(Method.GET,"/Users");
        Thread.sleep(3000);

    }

    @Test
    void checkResponseBody(){

       String responseBody=response.getBody().asString();
       System.out.println(" the responseBody"+responseBody);
        Assert.assertTrue(responseBody!=null);

    }

    @Test
    void checkStatusCode(){

     int statuscode=response.getStatusCode();
     System.out.println("The statuscode "+statuscode);
     Assert.assertEquals(statuscode,200);

    }
   @Test
     void checkResponseTime(){
        long responseTime=response.getTime();
           if(responseTime>2000)
       System.out.println("response time is grater than  2000");
         Assert.assertTrue(responseTime<2000);


   }
   @Test
    void checkStatusLine(){
        String statusLine=response.getStatusLine();
        System.out.println("statusline is "+statusLine);
        Assert.assertEquals(statusLine,"HTTP/1.1 200 OK");



   }
    @Test
    void checkContentType(){

    String contentType=response.header("Content-Type");
    System.out.println("The ContentType"+contentType);
    Assert.assertEquals(contentType,"application/json; charset=utf-8");

    }

    @Test
    void checkSeverType(){
    String servertype=response.header("server");
    System.out.println("the server is "+ servertype);
    Assert.assertEquals(servertype,"Microsoft-IIS/10.0");



    }


}
