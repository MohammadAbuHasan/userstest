package Testcaseuasers;

import PayloadsLookups.PostPayload;
import UsersUtilitis.Dataproviderdemo;
import Usersbase.userbase;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;


public class TC2_Post_Users {
      @BeforeClass
     void post(){
       userbase.sheetName="demo";
     }

    @Test(dataProvider = "excelData", dataProviderClass = Dataproviderdemo.class)
    void postUser(String username, String password, String ID){
        PostPayload obj = new PostPayload(username, password, ID);
        given().
                when().
                contentType("application/json").
                body(obj).
                post("https://fakerestapi.azurewebsites.net/api/Users ").
                then().
                assertThat().
                statusCode(200).
                log().body();


    }


}
