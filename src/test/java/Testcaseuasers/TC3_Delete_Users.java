package Testcaseuasers;

import UsersUtilitis.Dataproviderdemo;
import Usersbase.userbase;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class TC3_Delete_Users extends userbase {

    @BeforeClass
    void delete(){

        userbase.sheetName="Sheet1";

    }


    @Test(dataProvider = "excelData", dataProviderClass = Dataproviderdemo.class)
    void deleteUser(String ID, String Resp){

         String url="https://fakerestapi.azurewebsites.net/api/Users/"+ID;
        given().
                when().
                delete(url).
                then().
                assertThat().
                statusCode(200).
                log().body();


    }
}
